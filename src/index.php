<?php
/**
 * Created by PhpStorm.
 * User: adewale
 * Date: 27/04/18
 * Time: 12:28
 */

require __DIR__ . '/../vendor/autoload.php';

$courseId = $argv[1];
$accessToken  = $argv[2];

$canvasCCGenerator = new \CanvasImporter\CanvasCCGenerator($accessToken);

var_dump($canvasCCGenerator->importCourse($courseId));