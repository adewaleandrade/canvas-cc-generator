<?php
/**
 * Created by PhpStorm.
 * User: adewale
 * Date: 27/04/18
 * Time: 11:49
 */

namespace CanvasImporter\CanvasAPI;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class ApiClient
{
    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var string
     */
    private $apiBaseUrl = 'https://canvas.instructure.com/api/v1/';

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var array
     */
    private $defaultHeaders;

    /**
     * ApiClient constructor.
     * @param string $accessToken
     * @param string $apiBaseUrl
     */
    public function __construct($accessToken, $apiBaseUrl = null)
    {
        $this->accessToken = $accessToken;
        $apiBaseUrl && $this->apiBaseUrl = $apiBaseUrl;

        $this->defaultHeaders = ['Authorization' => "Bearer $accessToken"];

        $this->httpClient = new Client(['base_uri' => $this->apiBaseUrl]);
    }

    /**
     * @return Client
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @param null $courseId
     * @return mixed|null
     */
    public function getCourse($courseId = null) {
        $url = 'courses/';
        if ($courseId) {
            $url .= $courseId;
        }
        $response = $this->httpClient->get($url, ['headers' => $this->defaultHeaders]);

        if ($response->getStatusCode() == 200) {
            return \GuzzleHttp\json_decode($response->getBody()->getContents());
        }

        return null;
    }

    /**
     * @param int $courseId
     * @param int|null $assignmentId
     * @return null|string
     */
    public function getAssignments($courseId, $assignmentId = null) {
        $url = "courses/$courseId/assignments/";
        if ($assignmentId) {
            $url .= $assignmentId;
        }

        $response = $this->httpClient->get($url, ['headers' => $this->defaultHeaders]);

        if ($response->getStatusCode() == 200) {
            return \GuzzleHttp\json_decode($response->getBody()->getContents());
        }

        return null;
    }
}