<?php
/**
 * Created by PhpStorm.
 * User: adewale
 * Date: 27/04/18
 * Time: 16:20
 */

namespace CanvasImporter\CC;


use CanvasImporter\Course\Assignment;
use CanvasImporter\Course\Course;
use CanvasImporter\Course\Resource;
use GuzzleHttp\Client;
use Spatie\ArrayToXml\ArrayToXml;

class CCGenerator
{
    /**
     * @var array
     */
    private $resourceMap = [];

    /**
     * @var string
     */
    protected $coursePath = '';

    /**
     * @var array
     */
    protected $manifestArray = [];


    /**
     * @var string
     */
    protected $rootItemIdRef = '';


    /**
     * CCGenerator constructor.
     */
    public function __construct()
    {
        $this->manifestArray = [
            'metadata' => [
                'schema' => 'IMS Common Cartridge',
                'schemaversion' => '1.2.0',
                'lomimscc:lom' => [
                    'lomimscc:general' => [
                        'lomimscc:title' => [
                            'lomimscc:string' => 'Assignment Collection'
                        ]
                    ]
                ]
            ],
            'organizations' => [],
            'resources' => []
        ];
    }


    public function createFromCourse(Course $course)
    {
        $courseName = 'course_' . $course->getId() . '_' . uniqid();
        $courseCCFolderPath = __DIR__ . '/../../../tmp/' . $courseName;
        $this->coursePath = $courseCCFolderPath;

        mkdir($courseCCFolderPath);

        $this->createAssessment($course);
        $this->setManifestOrganization($course);
        $this->createManifestFile();
    }

    private function createAssessment(Course $course)
    {
        $assignments = $course->getAssignments();

        $httpClient = new Client(
            [
                'base_url' => ''
            ]
        );

        $resourcesFolder = $this->coursePath. '/resources';
        if (!file_exists($resourcesFolder)) {
            mkdir($resourcesFolder);
        }

        /** @var Assignment $assignment */
        foreach($assignments as $assignment) {
            $currentResources = count($this->manifestArray['resources']);
            $partialId = str_pad((string)($currentResources + 1) , 5, "0", STR_PAD_LEFT);
            $identifier = 'ccres' . $partialId;

            if (!$this->rootItemIdRef) {
                $this->rootItemIdRef = $identifier;
            }

            mkdir($this->coursePath . "/$identifier");

            $assignmentResource = [
                '_attributes' => [
                    "identifier" => $identifier,
                    'type' => 'webcontent',
                    'href' => "$identifier/$identifier.html",
                    'intendeduse' => "assignment",
                ],
                'file' => [
                    '_attributes' => [
                        'href' => "$identifier/$identifier.html",
                    ]
                ]
            ];
            $this->manifestArray['resources']['resource'][] = $assignmentResource;

            if (empty($assignment->getResources())) {
                file_put_contents($this->coursePath . "/$identifier/$identifier.html", $assignment->getDescription());
                continue;
            }

            /** @var Resource $resource */
            foreach ($assignment->getResources() as $resource) {
                $filename = uniqid();
                $resourcePath = $resourcesFolder . "/$filename";
                $file = $httpClient->get($resource->getHref(),['save_to' => $resourcePath]);

                //checkStatus
                $this->resourceMap[$resource->getHref()] = '$IMS-CC-FILEBASE$../' . "resoureces/$filename";
                $this->manifestArray['resources']['resource'][] = [
                    '_attributes' => [
                        "identifier" => $resource->getIdentifier(),
                        'type' => 'webcontent',
                        'href' => "resoureces/$filename",
                    ],
                    'file' => [
                        '_attributes' => [
                            'href' => "resoureces/$filename",
                        ]
                    ]
                ];
            }

            $assignmentDescription = $assignment->getDescription();
            foreach ($this->resourceMap as $needle => $newRef) {
                $assignmentDescription = str_replace($needle, $newRef, $assignmentDescription);
            }

            file_put_contents($this->coursePath . "/$identifier/$identifier.html", $assignmentDescription);
        }
    }

    private function setManifestOrganization(Course $course)
    {
        $this->manifestArray['organizations'] = [
            'organization' => [
                '_attributes' => ['identifier' => 'org', 'structure' => 'rooted-hierarchy'],
                'item' => [
                    '_attributes' => ['identifier' => 'root'],
                    'item' => [
                        '_attributes' => ['identifier' => $course->getId(), 'identifierref' => $this->rootItemIdRef],
                        'title' => $course->getTitile()
                    ]
                ]
            ]
        ];
    }

    private function createManifestFile()
    {
        $manifestXml = ArrayToXml::convert($this->manifestArray, [
            'rootElementName' => 'manifest',
            '_attributes' => [
                'xmlns' => "http://www.imsglobal.org/xsd/imsccv1p2/imscp_v1p1",
                'xmlns:lom' => "http://ltsc.ieee.org/xsd/imsccv1p2/LOM/resource",
                'xmlns:lomimscc' => "http://ltsc.ieee.org/xsd/imsccv1p2/LOM/manifest",
                'xmlns:xsi' => "http://www.w3.org/2001/XMLSchema-instance",
                'identifier' => "cctd0001",
                'xsi:schemaLocation' => "http://www.imsglobal.org/xsd/imsccv1p2/imscp_v1p1 http://www.imsglobal.org/profile/cc/ccv1p2/ccv1p2_imscp_v1p2_v1p0.xsd http://www.imsglobal.org/xsd/imsccv1p2/imsccauth_v1p2 http://www.imsglobal.org/profile/cc/ccv1p2/ccv1p2_imsccauth_v1p2.xsd http://ltsc.ieee.org/xsd/imsccv1p2/LOM/manifest http://www.imsglobal.org/profile/cc/ccv1p2/LOM/ccv1p2_lommanifest_v1p0.xsd http://ltsc.ieee.org/xsd/imsccv1p2/LOM/resource http://www.imsglobal.org/profile/cc/ccv1p2/LOM/ccv1p2_lomresource_v1p0.xsd http://www.imsglobal.org/xsd/ims_qtiasiv1p2 http://www.imsglobal.org/profile/cc/ccv1p2/ccv1p2_qtiasiv1p2p1_v1p0.xsd http://www.imsglobal.org/xsd/imsccv1p2/imswl_v1p2 http://www.imsglobal.org/profile/cc/ccv1p2/ccv1p2_imswl_v1p2.xsd http://www.imsglobal.org/xsd/imsccv1p2/imsdt_v1p2 http://www.imsglobal.org/profile/cc/ccv1p2/ccv1p2_imsdt_v1p2.xsd http://www.imsglobal.org/xsd/imsccv1p2/imscsmd_v1p0 http://www.imsglobal.org/profile/cc/ccv1p2/ccv1p2_imscsmd_v1p0.xsd http://www.imsglobal.org/xsd/imslticc_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticc_v1p0p1.xsd http://www.imsglobal.org/xsd/imslticp_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticp_v1p0.xsd http://www.imsglobal.org/xsd/imslticm_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imslticm_v1p0.xsd http://www.imsglobal.org/xsd/imsbasiclti_v1p0 http://www.imsglobal.org/xsd/lti/ltiv1p0/imsbasiclti_v1p0p1.xsd"
            ]
        ]);
        file_put_contents($this->coursePath . "/imsmanifest.xml", $manifestXml);
    }
}