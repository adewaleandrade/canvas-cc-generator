<?php
/**
 * Created by PhpStorm.
 * User: adewale
 * Date: 27/04/18
 * Time: 10:32
 */

namespace CanvasImporter;

use CanvasImporter\CanvasAPI\ApiClient;
use CanvasImporter\CC\CCGenerator;
use CanvasImporter\Course\Course;
use CanvasImporter\Course\CourseImporter;
use Guzzle\Http\Client;

class CanvasCCGenerator
{
    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var string
     */
    private $apiBaseUrl = 'https://canvas.instructure.com/api/v1/';

    /**
     * @var Client
     */
    protected $apiClient;

    /**
     * @var CourseImporter
     */
    protected $courseImporter;



    /**
     * CanvasCCGenerator constructor.
     * @param string $accessToken
     * @param string $apiBaseUrl
     */
    public function __construct($accessToken, $apiBaseUrl = null)
    {
        $this->accessToken = $accessToken;
        $apiBaseUrl && $this->apiBaseUrl = $apiBaseUrl;

        $this->apiClient = new ApiClient($accessToken, $this->apiBaseUrl);

        $this->courseImporter = new CourseImporter($this->apiClient);
    }

    public function importCourse($courseId)
    {
        $canvasCourse = $this->apiClient->getCourse($courseId);

        $course = new Course($canvasCourse->id, $canvasCourse->name);
        $course->setAssignmentsFromApiData(
         $this->apiClient->getAssignments($canvasCourse->id)
        );

        $ccGenerator = new CCGenerator();
        $ccGenerator->createFromCourse($course);

       return true;
    }


}