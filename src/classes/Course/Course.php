<?php
/**
 * Created by PhpStorm.
 * User: adewale
 * Date: 27/04/18
 * Time: 13:28
 */

namespace CanvasImporter\Course;


class Course
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $titile;

    /**
     * @var string
     */
    private $description;

    /**
     * @var Assignment[]
     */
    private $assignments;

    /**
     * Course constructor.
     * @param int $id
     * @param string $titile
     */
    public function __construct($id, $titile)
    {
        $this->id = $id;
        $this->titile = $titile;

        $this->assignments = [];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitile()
    {
        return $this->titile;
    }

    /**
     * @param string $titile
     */
    public function setTitile($titile)
    {
        $this->titile = $titile;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Assignment[]
     */
    public function getAssignments()
    {
        return $this->assignments;
    }

    /**
     * @param Assignment[] $assignments
     */
    public function setAssignments(Assignment $assignments)
    {
        $this->assignments = $assignments;
    }

    /**
     * @param Assignment $assignment
     */
    public function addAssignment(Assignment $assignment)
    {
        $this->assignments[] = $assignment;
    }

    public function setAssignmentsFromApiData($apiAssignments)
    {
        foreach ($apiAssignments as $apiAssignment) {
            $assignment = new Assignment(
                $this->id,
                $apiAssignment->name,
                $apiAssignment->description
            );
            $assignment->parseResources();
            $this->addAssignment($assignment);
        }
    }
}