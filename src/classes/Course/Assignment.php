<?php
/**
 * Created by PhpStorm.
 * User: adewale
 * Date: 27/04/18
 * Time: 13:28
 */

namespace CanvasImporter\Course;


class Assignment
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $courseId;

    /**
     * @var Resource[]
     */
    private $resources;

    /**
     * Assignment constructor.
     * @param int $courseId
     * @param string $name
     * @param string $description
     */
    public function __construct($courseId, $name, $description = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->courseId = $courseId;

        $this->resources = [];
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getCourseId()
    {
        return $this->courseId;
    }

    /**
     * @param int $courseId
     */
    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
    }

    /**
     * @return \Resource[]
     */
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * @param \Resource[] $resources
     */
    public function setResources($resources)
    {
        $this->resources = $resources;
    }

    /**
     * @param Resource $resource
     */
    public function addResource(Resource $resource)
    {
        $this->resources[] = $resource;
    }


    public function parseResources()
    {
        if (!$this->description) {
            return;
        }

        preg_match_all('/src\s*=\\s*"(.+?)"/', $this->description, $matches);

        foreach ($matches[1] as $resourceRef) {
            $resource = new Resource('asnmt_' . uniqid(), 'webcontent', $resourceRef);
            $this->addResource($resource);
        }
    }
}