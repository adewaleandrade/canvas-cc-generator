FROM phpearth/php:7.2-nginx

RUN apk add --no-cache php7.2-sodium php7.2-intl php7.2-pdo_mysql
RUN apk add --no-cache composer
RUN apk add --no-cache phpunit
RUN apk add --no-cache bash

RUN mkdir /canvas-importer
WORKDIR /canvas-importer

COPY . /canvas-importer